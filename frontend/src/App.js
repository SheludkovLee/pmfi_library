import AuthPage from "./pages/auth/auth.page";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import CreateBook from "./pages/create-book/create-book.page";
import Home from "./pages/home/home.page";

function App() {
  return (
      <BrowserRouter>
          <Routes>
              <Route path={'/'} element={<AuthPage />}/>
              <Route path={'home'} element={<Home />}/>
              <Route path={'create-book'} element={<CreateBook />}/>
          </Routes>
      </BrowserRouter>
  );
}

export default App;
