import React from "react";
import { IBook } from "../types/book.interface";


export function Book({ title, author, publisher, description }: IBook) {
    return <React.Fragment>
        <p>Название: {title}</p>
        <p>Автор: {author.name}</p>
        <p>Издатель: {publisher?.name || '---'}</p>
        <p>Аннотация: {description || 'Описание отсутсвует'}</p>
    </React.Fragment>
}