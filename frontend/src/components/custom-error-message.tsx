import { ErrorMessage } from "@hookform/error-message";
import '../common/styles/error-message.css';

interface ErrorMessageProps {
    errors: any,
    name: string,
}

export function CustomErrorMessage({errors, name}: ErrorMessageProps) {
    return <ErrorMessage
        errors={errors}
        name={name}
        render={({ messages }) => {
            return messages
                ? Object.entries(messages).map(([type, message]: [string, string]) => (
                    <p key={type} className='error-message'>{message}</p>
                ))
                : null;
        }}
    />
}