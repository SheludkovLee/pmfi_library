import {useNavigate} from "react-router";

export default function Content({ children }) {
    const navigate = useNavigate();
    return <main style={{ margin: '20px', padding: '10px' }}>{children}</main>
}