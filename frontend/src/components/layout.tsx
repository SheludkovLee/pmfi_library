import React from "react";
import Header from "./header";
import Content from "./content";
import Footer from "./footer";

export function Layout({ children }) {
    return <React.Fragment>
        <Header></Header>
        <Content>{children}</Content>
        <Footer></Footer>
    </React.Fragment>
}