import {useNavigate} from "react-router";
import React from "react";

export default function Header() {
    const navigate = useNavigate();

    const handleSelect = (e: React.MouseEvent | React.FormEvent)  => {
        if (e.target['id'] === 'catalog') {
            return navigate('/home');
        }
        navigate('/create-book');
    }

    return <header>
        <div style={{display: 'block', marginLeft: '20px', cursor: 'pointer'}}>
            <nav id='catalog' style={{ display: 'inline', padding: '10px'}} onClick={handleSelect}>Каталог</nav>
            <nav id='create' style={{ display: 'inline'}} onClick={handleSelect}>Загрузить</nav>
        </div>
    </header>
}