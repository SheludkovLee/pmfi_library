import {gql} from "@apollo/client";

export const GET_BOOKS = gql`
    query findItems {
        findItems {
            id
            title
            author {
                name
            }
            publisher {
                name
            }
            description
        }
    }
`;