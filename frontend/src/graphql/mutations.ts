import {gql} from "@apollo/client";

export const SIGN_IN = gql`
    mutation signIn($signInInput: SignInInput!) {
        signIn(signInInput: $signInInput) {
            jwtToken
        }
    }
`;

export const SIGN_UP = gql`
    mutation signUp($signUpInput: SignUpInput!) {
        signUp(signUpInput: $signUpInput) {
            jwtToken
            user {
                id
                username
            }
        }
    }
`;

export const CREATE_BOOK = gql`
    mutation createItem($createItemInput: CreateItemInput!) {
        createItem(createItemInput: $createItemInput) {
            id
            title
            author {
                name
            }
            publisher {
                name
            }
            description
        }
    }
`;