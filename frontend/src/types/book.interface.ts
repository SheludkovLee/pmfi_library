
export interface IBook {
    id: string;
    title: string;
    author: {
        name: string;
    };
    publisher: {
        name: string;
    }
    description: string;
}