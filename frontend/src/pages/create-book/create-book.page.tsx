import React from "react";
import { useForm } from "react-hook-form";
import ValidationOptions from './validation/validation.options';
import { CustomErrorMessage } from "../../components/custom-error-message";
import { useMutation } from "@apollo/client";
import { CREATE_BOOK } from "../../graphql/mutations";
import {Layout} from "../../components/layout";

export default function CreateBook() {
    const { register, handleSubmit, formState: { errors } } = useForm({
        criteriaMode: 'all',
    });
    const [ createItemMutation ] = useMutation(CREATE_BOOK, { errorPolicy: 'all'});

    const onSubmit = async (input) => {
        console.log(input);
        const { errors } = await createItemMutation({ variables: {
            createItemInput: input
        }});
        if (errors) {
            errors.forEach(err => alert(err.extensions?.['response']?.['message']));
        } else {
            alert('Материал успешно добавлен');
        }
    }


    return (
        <Layout>
        <div className="form-wrapper">
            <h1>Загрузить материал</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label htmlFor="Electronic">Электронный</label>
                <input id="Electronic" {...register("isElectronic")} type="radio" value={'true'} defaultChecked/>

                <label htmlFor="Physical">Физический</label>
                <input id="Physical" {...register("isElectronic")} type="radio" value={'false'} />

                <p>Тип:</p>
                <select {...register("type")}>
                    <option value="BOOK" label="Книга"></option>
                    <option value="ARTICLE" label="Статья"></option>
                </select>

                <p>Название</p>
                <input type="text" placeholder="Название" {...register("title", ValidationOptions.title)} />
                <CustomErrorMessage errors={errors} name='title'/>

                <p>Автор</p>
                <input type="text" placeholder="Автор" {...register("author", ValidationOptions.author)} />
                <CustomErrorMessage errors={errors} name='author'/>

                <p>Издатель</p>
                <input type="text" placeholder="Издатель" {...register("publisher", ValidationOptions.publisher)} />
                <CustomErrorMessage errors={errors} name='publisher'/>

                <p>Год издания</p>
                <input type="text" placeholder={"1999"} {...register("publicationYear", ValidationOptions.publicationYear)}/>

                <p>Описание</p>
                <textarea placeholder='Описание' {...register("description", ValidationOptions.description)} />
                <CustomErrorMessage errors={errors} name='description'/>

                <div>
                    <input type="submit" />
                </div>
            </form>
        </div>
        </Layout>
    )
}