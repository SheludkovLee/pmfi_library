import { RegisterOptions } from "react-hook-form";

export default class CreateBookFormValidationOptions {
    static readonly title: RegisterOptions = {
        required: 'Название обязательно',
        maxLength: {
            value: 100,
            message: 'Превышена максимальная длина',
        }
    }

    static readonly author: RegisterOptions = {
        required: 'Автор обязателен',
    }

    static readonly publisher: RegisterOptions = {
        maxLength: {
            value: 100,
            message: 'Превышена максимальная длина'
        }
    }

    static readonly publicationYear: RegisterOptions = {
        valueAsNumber: true,
    }

    static readonly description: RegisterOptions = {
        maxLength: {
            value: 250,
            message: 'Превышена максимальная длина'
        }
    }
}