import React from "react";
import { GET_BOOKS } from "../../graphql/queries";
import { useQuery } from "@apollo/client";
import {IBook} from "../../types/book.interface";
import {Book} from "../../components/book";
import {Layout} from "../../components/layout";

export default function Home() {
    const { loading, data, error } = useQuery(GET_BOOKS);

    if (loading) {
        return <p>'Loading...</p>;
    }

    if (error) {
        return <p>error.message</p>;
    }

    return <Layout>
        <h1>Библиотека ПМФИ</h1>
        {data.findItems.map((item: IBook) => (<div key={item.id}><Book key={item.id} {...item}/> <p><br/></p></div>))}
    </Layout>
}
