import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import {SIGN_IN, SIGN_UP} from "../../graphql/mutations";
import {useNavigate} from "react-router";

export default function AuthPage() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const navigate = useNavigate();

    const [ signUpMutation ] = useMutation(SIGN_UP);
    const [ signInMutation ] = useMutation(SIGN_IN);

    const handleSignIn = async (e: React.MouseEvent | React.FormEvent) => {
        e.preventDefault();
        try {
            const { data } = await signInMutation({ variables: { signInInput: { username, password} }});
            localStorage.setItem('token', data.signIn.jwtToken);
            navigate('home');
        } catch (e) {
            alert(e);
        }
    }

    const handleSignUp = async (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        try {
            const { data } = await signUpMutation({variables: {signUpInput: {username, password}}});
            localStorage.setItem('token', data.signUp.jwtToken);
            navigate('home');
        } catch (e) {
            alert(e);
        }
    }

    return(
        <div className="login-wrapper">
            <h1>Авторизация</h1>
            <form onSubmit={handleSignIn}>
                <label>
                    <p>Логин</p>
                    <input type="text" value={username} onChange={e => setUsername(e.target.value)}/>
                </label>
                <label>
                    <p>Пароль</p>
                    <input type="password" value={password} onChange={e => setPassword(e.target.value)}/>
                </label>
                <div style={{ margin: '10px'}}>
                    <button style={{ marginRight: '10px'}} type="submit" onClick={handleSignIn}>SignIn</button>
                    <button type="submit" onClick={handleSignUp}>SignUp</button>
                </div>
            </form>
        </div>
    )
}