import { Module } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentsRepository } from './comments.repository';
import { CommentsMutationResolver } from './resolvers/comments.mutation-resolver';
import { UsersRepository } from 'src/users/users.repository';

@Module({
  imports: [TypeOrmModule.forFeature([CommentsRepository, UsersRepository])],
  providers: [CommentsService, CommentsMutationResolver],
  exports: [CommentsService],
})
export class CommentsModule {}
