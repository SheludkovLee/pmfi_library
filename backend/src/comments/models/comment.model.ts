import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from 'src/common';

@ObjectType('Comment', {
  description: 'Модель комментария',
})
export class CommentModel extends BaseModel {
  @Field({ description: 'Текст комментария' })
  text: string;

  @Field(() => ID, { description: 'ID автора комментария' })
  userId: string;

  @Field(() => ID, {
    description: 'ID ресурса, на который оставили комментарий',
  })
  itemId: string;
}
