import { Field, ObjectType } from '@nestjs/graphql';
import { CommentModel } from './comment.model';
import { PaginationModel } from 'src/common';

@ObjectType('PaginationComments', {
  description: 'Модель пагинированных комментариев',
})
export class PaginationCommentsModel extends PaginationModel {
  @Field(() => [CommentModel], { description: 'Массив комментариев' })
  items: CommentModel[];
}
