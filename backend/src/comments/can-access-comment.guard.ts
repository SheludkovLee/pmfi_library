import { CanAccessGuard } from '../common/guards/can-access.guard';
import { Comment } from './comment.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CanAccessCommentGuard extends CanAccessGuard<Comment> {
  constructor() {
    super(Comment);
  }
}
