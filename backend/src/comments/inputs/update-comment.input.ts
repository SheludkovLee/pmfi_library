import { InputType, OmitType, PartialType } from '@nestjs/graphql';
import { CreateCommentInput } from './create-comment.input';

@InputType({ description: 'Ипут на обновление комментария' })
export class UpdateCommentInput extends PartialType(
  OmitType(CreateCommentInput, ['itemId']),
) {}
