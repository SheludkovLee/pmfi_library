import { Field, ID, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsString, IsUUID, Length } from 'class-validator';
import { Transform } from 'class-transformer';

@InputType({ description: 'Инпут на создание комментария' })
export class CreateCommentInput {
  @Field({ description: 'Текст комментария' })
  @IsNotEmpty({ message: 'Текст комментария не может быть пустым' })
  @IsString()
  @Length(3, 250, { message: 'Недопустимая длина комментария' })
  @Transform(({ value }) => value.trim().toLowerCase())
  text: string;

  @Field(() => ID, {
    description: 'ID ресурса, на который оставить комментарий',
  })
  @IsNotEmpty()
  @IsUUID()
  itemId: string;
}
