import { NestDataLoader } from '@pmfi/dataloader';
import { CommentModel } from './models/comment.model';
import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { CommentsRepository } from './comments.repository';

@Injectable()
export class CommentLoader implements NestDataLoader<string, CommentModel> {
  constructor(private commentsRepository: CommentsRepository) {}

  generateDataLoader(): DataLoader<string, CommentModel> {
    return new DataLoader<string, CommentModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const comments = await this.commentsRepository.findByIds(
        uniqueKeys as string[],
      );

      return keys.map(
        (key) =>
          comments.find((comment) => comment.id === key) ||
          new Error(`Could not load comment with id: ${key}`),
      );
    });
  }
}
