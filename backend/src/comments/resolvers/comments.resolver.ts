import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { CommentModel } from '../models/comment.model';
import { Loader } from '@pmfi/dataloader';
import { UserLoader } from 'src/users/user.loader';
import DataLoader from 'dataloader';
import { UserModel } from 'src/users/user.model';

@Resolver(() => CommentModel)
export class CommentsResolver {
  @ResolveField(() => UserModel, { description: 'Автор комментария' })
  async user(
    @Parent() comment: CommentModel,
    @Loader(UserLoader) userLoader: DataLoader<string, UserModel>,
  ): Promise<UserModel> {
    const { userId } = comment;
    return userLoader.load(userId);
  }
}
