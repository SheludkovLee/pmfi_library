import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { CommentModel } from '../models/comment.model';
import { CommentsService } from '../comments.service';
import { Self } from 'src/common';
import { CreateCommentInput } from '../inputs/create-comment.input';
import { UseGuards } from '@nestjs/common';
import { CanAccessCommentGuard } from '../can-access-comment.guard';
import { UpdateCommentInput } from '../inputs/update-comment.input';

@Resolver(() => CommentModel)
export class CommentsMutationResolver {
  constructor(private commentsService: CommentsService) {}

  @Mutation(() => CommentModel, { description: 'Создать комментарий' })
  async createComment(
    @Self('id') userId: string,
    @Args('createCommentInput') input: CreateCommentInput,
  ): Promise<CommentModel> {
    return this.commentsService.create(userId, input);
  }

  @Mutation(() => CommentModel, { description: 'Изменить комментарий' })
  @UseGuards(CanAccessCommentGuard)
  async updateComment(
    @Args('id') id: string,
    @Args('updateCommentInput') input: UpdateCommentInput,
  ): Promise<CommentModel> {
    return this.commentsService.update(id, input);
  }

  @Mutation(() => Boolean, { description: 'Удалить комментарий' })
  @UseGuards(CanAccessCommentGuard)
  async removeComment(@Args('id') id: string): Promise<boolean> {
    return this.commentsService.remove(id);
  }
}
