import { Injectable } from '@nestjs/common';
import { Comment } from './comment.entity';
import { CreateCommentInput } from './inputs/create-comment.input';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { CommentsRepository } from './comments.repository';
import { UpdateCommentInput } from './inputs/update-comment.input';
import { SortInput } from '../common';

@Injectable()
export class CommentsService {
  constructor(private commentsRepository: CommentsRepository) {}

  async create(userId: string, input: CreateCommentInput): Promise<Comment> {
    return this.commentsRepository.create({ userId, ...input });
  }

  async findOne(id: string): Promise<Comment> {
    return this.commentsRepository.findOneOrFail(id);
  }

  async findAll(
    itemId: string,
    paginationOptions: IPaginationOptions,
    { sort, order }: SortInput,
  ): Promise<Pagination<Comment>> {
    const qb = this.commentsRepository.createQueryBuilder();
    qb.andWhere({ itemId: itemId });
    qb.orderBy(sort, order);

    return paginate<Comment>(qb, paginationOptions);
  }

  async update(id: string, input: UpdateCommentInput): Promise<Comment> {
    await this.commentsRepository.update(id, input);
    return this.findOne(id);
  }

  async remove(id: string): Promise<boolean> {
    const comment = await this.findOne(id);
    await this.commentsRepository.remove(comment);
    return true;
  }
}
