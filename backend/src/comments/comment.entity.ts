import { BaseEntity } from 'src/common';
import { Column, Entity, ManyToOne } from 'typeorm';
import { User } from 'src/users/user.entity';
import { Item } from 'src/items/entities/item.entity';

const tableName = 'comments';

@Entity(tableName, { orderBy: { createdAt: 'ASC' } })
export class Comment extends BaseEntity {
  static tableName = tableName;
  @Column('varchar', { length: 250 })
  text: string;

  @Column('uuid')
  userId: string;

  @Column('uuid')
  itemId: string;

  @ManyToOne(() => User, (user) => user.comments, {
    onDelete: 'CASCADE',
  })
  user: User;

  @ManyToOne(() => Item, (item) => item.comments, {
    onDelete: 'CASCADE',
  })
  item: Item;
}
