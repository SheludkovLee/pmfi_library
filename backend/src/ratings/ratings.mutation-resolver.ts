import { RatingsService } from './ratings.service';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { RatingModel } from './rating.model';
import { Self } from 'src/common';
import { CreateRatingInput } from './create-rating.input';
import { ParseUUIDPipe } from '@nestjs/common';

@Resolver(() => RatingModel)
export class RatingsMutationResolver {
  constructor(private ratingsService: RatingsService) {}

  @Mutation(() => Boolean, { description: 'Поставить оценку' })
  async addRatingToItem(
    @Self('id') userId: string,
    @Args('createRatingInput') input: CreateRatingInput,
  ): Promise<boolean> {
    await this.ratingsService.create(userId, input);
    return true;
  }

  @Mutation(() => Boolean, { description: 'Удалить оценку' })
  async removeItemRating(
    @Self('id') userId: string,
    @Args('itemId', ParseUUIDPipe) itemId: string,
  ): Promise<boolean> {
    return this.ratingsService.remove(userId, itemId);
  }
}
