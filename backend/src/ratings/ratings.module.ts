import { Module } from '@nestjs/common';
import { RatingsService } from './ratings.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RatingsRepository } from './ratings.repository';
import { RatingsMutationResolver } from './ratings.mutation-resolver';

@Module({
  imports: [TypeOrmModule.forFeature([RatingsRepository])],
  providers: [RatingsService, RatingsMutationResolver],
  exports: [RatingsService],
})
export class RatingsModule {}
