import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { User } from 'src/users/user.entity';
import { Item } from 'src/items/entities/item.entity';

const tableName = 'ratings';

@Entity(tableName, { orderBy: { createdAt: 'DESC' } })
@Unique(['userId', 'itemId'])
export class Rating {
  static tableName = tableName;

  @PrimaryColumn('uuid')
  userId: string;

  @PrimaryColumn('uuid')
  itemId: string;

  @Column('int2')
  value: number;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @ManyToOne(() => User, (user) => user.ratings, {
    onDelete: 'CASCADE',
  })
  user: User;

  @ManyToOne(() => Item, (item) => item.ratings, {
    onDelete: 'CASCADE',
  })
  item: Item;
}
