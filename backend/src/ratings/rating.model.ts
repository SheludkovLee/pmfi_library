import { Field, ID, Int, ObjectType } from '@nestjs/graphql';

@ObjectType('Rating', { description: 'Модель оценки ресурса' })
export class RatingModel {
  @Field(() => ID, { description: 'ID пользователя, поставившего оценку' })
  userId: string;

  @Field(() => ID, { description: 'ID ресурса, которому поставили оценку' })
  itemId: string;

  @Field(() => Int, { description: 'Численное значение оценки' })
  value: number;
}
