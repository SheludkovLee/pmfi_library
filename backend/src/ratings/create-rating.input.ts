import { Field, InputType } from '@nestjs/graphql';
import { IsIn, IsInt, IsNotEmpty, IsUUID } from 'class-validator';

@InputType({ description: 'Инпут на создание оценки' })
export class CreateRatingInput {
  @Field({ description: 'ID ресурса, которому ставится оценка' })
  @IsNotEmpty()
  @IsUUID()
  itemId: string;

  @Field({ description: 'Численное значение оценки' })
  @IsNotEmpty()
  @IsInt()
  @IsIn([1, 2, 3, 4, 5])
  value: number;
}
