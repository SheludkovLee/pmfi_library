import { Injectable } from '@nestjs/common';
import { RatingsRepository } from './ratings.repository';
import { CreateRatingInput } from './create-rating.input';
import { Rating } from './rating.entity';

@Injectable()
export class RatingsService {
  constructor(private ratingsRepository: RatingsRepository) {}

  async create(userId: string, input: CreateRatingInput): Promise<Rating> {
    return this.ratingsRepository.save({ userId, ...input });
  }

  async findOne(userId: string, itemId: string): Promise<Rating> {
    return this.ratingsRepository.findOne({ userId, itemId });
  }

  async remove(userId: string, itemId: string): Promise<boolean> {
    const rating = await this.ratingsRepository.findOneOrFail({
      where: {
        userId,
        itemId,
      },
    });
    await this.ratingsRepository.remove(rating);
    return true;
  }

  async calculateAverageRating(itemId: string): Promise<number> {
    const { avg } = await this.ratingsRepository
      .createQueryBuilder('ratings')
      .where({ itemId: itemId })
      .select('AVG(ratings.value)', 'avg')
      .getRawOne();
    return Number(avg);
  }
}
