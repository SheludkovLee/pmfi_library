import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from 'src/app.module';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { graphqlUploadExpress } from 'graphql-upload';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService: ConfigService = app.get(ConfigService);

  app.enableCors({ origin: '*', credentials: true });
  app.useGlobalPipes(new ValidationPipe({ transform: true, always: true }));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  app.use(graphqlUploadExpress());

  const PORT = configService.get<number>('PORT');

  await app.listen(PORT, () => console.log(`Server started on port: ${PORT}`));
}

bootstrap();
