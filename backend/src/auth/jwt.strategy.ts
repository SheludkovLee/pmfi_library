import {PassportStrategy} from '@nestjs/passport';
import {ExtractJwt, Strategy} from 'passport-jwt';
import {ConfigService} from '@nestjs/config';
import {Injectable} from '@nestjs/common';
import {UsersRepository} from '../users/users.repository';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private configService: ConfigService,
        private usersRepo: UsersRepository,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get('JWT_SECRET_KEY') || 'secret-key',
        });
    }

    async validate(payload: { sub: string; username: string }) {
        const user = await this.usersRepo.findOne(payload.sub);
        if (!user) {
            return null;
        }
        return {id: payload.sub, username: payload.username};
    }
}
