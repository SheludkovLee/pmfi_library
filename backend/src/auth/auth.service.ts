import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/services/users.service';
import { HashService } from 'src/users/services/hash.service';
import { User } from 'src/users/user.entity';
import { SignUpInput } from 'src/auth/inputs/sign-up.input';
import { SignInInput } from './inputs/sign-in.input';
import { SignInModel } from './models/sign-in.model';
import { SignUpModel } from './models/sign-up.model';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private hashService: HashService,
    private jwtService: JwtService,
  ) {}

  async signInUser({ username, password }: SignInInput): Promise<SignInModel> {
    const user = await this.usersService.findOneByOptions({
      where: {
        username,
      },
    });

    if (!user) {
      throw new NotFoundException(
        `User with username: ${username} doesn't exist`,
      );
    }

    const isRealPassword = await this.hashService.compareHash(
      password,
      user.password,
    );

    if (!isRealPassword) {
      throw new UnauthorizedException('Unauthorized. Please try again.');
    }
    const token = this.getJWT(user);
    console.log(token);
    return {
      jwtToken: token,
    };
  }

  async signUpUser(input: SignUpInput): Promise<SignUpModel> {
    const candidate = await this.usersService.findOneByOptions({
      where: { username: input.username },
    });

    if (candidate) {
      throw new BadRequestException(
        `User with username: ${candidate.username} already exists`,
      );
    }

    const newUser = await this.usersService.createOne(input);
    const jwtToken = this.getJWT(newUser);

    return {
      jwtToken,
      user: newUser,
    };
  }

  private getJWT(user: User): string {
    const payload = { username: user.username, sub: user.id };
    return this.jwtService.sign(payload);
  }
}
