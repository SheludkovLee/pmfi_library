import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType('SignIn', { description: 'Модель авторизации пользователя' })
export class SignInModel {
  @Field({ description: 'JWT-токен для аутентификации пользователя' })
  jwtToken: string;
}
