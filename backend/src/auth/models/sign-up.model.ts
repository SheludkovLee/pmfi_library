import { Field, ObjectType } from '@nestjs/graphql';
import { UserModel } from 'src/users/user.model';
import { SignInModel } from './sign-in.model';

@ObjectType('SignUp', { description: 'Модель регистрации пользователя' })
export class SignUpModel extends SignInModel {
  @Field(() => UserModel, { description: 'Зарегистрированный пользователь' })
  user: UserModel;
}
