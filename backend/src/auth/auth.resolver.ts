import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { SignInModel } from './models/sign-in.model';
import { SignInInput } from './inputs/sign-in.input';
import { SignUpModel } from './models/sign-up.model';
import { SignUpInput } from 'src/auth/inputs/sign-up.input';
import { Public } from 'src/common/decorators/public.decorator';

@Resolver()
@Public()
export class AuthResolver {
  constructor(private authService: AuthService) {}

  @Mutation(() => SignInModel, { description: 'Авторизоваться' })
  signIn(@Args('signInInput') input: SignInInput): Promise<SignInModel> {
    return this.authService.signInUser(input);
  }

  @Mutation(() => SignUpModel, { description: 'Зарегистрироваться' })
  signUp(@Args('signUpInput') input: SignUpInput): Promise<SignUpModel> {
    return this.authService.signUpUser(input);
  }
}
