import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsString, Length } from 'class-validator';
import { Transform } from 'class-transformer';

@InputType({ description: 'Инпут на регистрацию пользователя' })
export class SignUpInput {
  @Field({ description: 'Имя пользователя' })
  @IsNotEmpty({ message: 'Имя пользователя не может быть пустым' })
  @IsString()
  @Length(3, 50, { message: 'Недопустимая длина для имени пользователя' })
  @Transform(({ value }) => value.trim().toLowerCase())
  username: string;

  @Field({ description: 'Пароль пользователя' })
  @IsNotEmpty({ message: 'Пароль не может быть пустым' })
  @IsString()
  @Transform(({ value }) => value.trim())
  password: string;
}
