import { InputType, PickType } from '@nestjs/graphql';
import { SignUpInput } from './sign-up.input';

@InputType({ description: 'Инпут на авторизацию пользователя' })
export class SignInInput extends PickType(SignUpInput, [
  'username',
  'password',
]) {}
