import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  Unique,
} from 'typeorm';
import { User } from 'src/users/user.entity';
import { Item } from 'src/items/entities/item.entity';

const tableName = 'favorites';

@Entity(tableName, { orderBy: { createdAt: 'DESC' } })
@Unique(['userId', 'itemId'])
export class Favorite {
  static tableName = 'favorites';

  @PrimaryColumn('uuid')
  userId: string;

  @PrimaryColumn('uuid')
  itemId: string;

  @CreateDateColumn()
  createdAt: string;

  @ManyToOne(() => User, (user) => user.favorites, {
    onDelete: 'CASCADE',
  })
  user: User;

  @ManyToOne(() => Item, (item) => item.favorites, {
    onDelete: 'CASCADE',
  })
  item: Item;
}
