import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { FavoriteModel } from './favorite.model';
import { FavoritesService } from './favorites.service';
import { Self } from 'src/common/decorators/self.decorator';
import { ParseUUIDPipe } from '@nestjs/common';

@Resolver(() => FavoriteModel)
export class FavoritesMutationResolver {
  constructor(private favoritesService: FavoritesService) {}

  @Mutation(() => Boolean, { description: 'Добавить ресурс в избранное' })
  async addItemToFavorite(
    @Self('id') userId: string,
    @Args('itemId', ParseUUIDPipe) itemId: string,
  ): Promise<boolean> {
    await this.favoritesService.createFavorite(userId, itemId);
    return true;
  }

  @Mutation(() => Boolean, { description: 'Удалить ресурс из избранного' })
  async removeItemFromFavorite(
    @Self('id') userId: string,
    @Args('itemId', ParseUUIDPipe) itemId: string,
  ): Promise<boolean> {
    return this.favoritesService.removeFavorite(userId, itemId);
  }
}
