import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType('Favorite', { description: 'Модель избранного ресурса' })
export class FavoriteModel {
  @Field(() => ID, {
    description: 'ID пользователя, добавившего ресурс в избранное',
  })
  userId: string;

  @Field(() => ID, { description: 'ID ресурса, добавленного в избранное' })
  itemId: string;
}
