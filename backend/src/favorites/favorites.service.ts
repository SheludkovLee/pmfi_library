import { Injectable } from '@nestjs/common';
import { FavoritesRepository } from './favorites.repository';
import { Favorite } from './favorite.entity';

@Injectable()
export class FavoritesService {
  constructor(private favoritesRepository: FavoritesRepository) {}

  async createFavorite(userId: string, itemId: string): Promise<Favorite> {
    return this.favoritesRepository.save({ userId, itemId });
  }

  async findFavorite(userId: string, itemId: string): Promise<Favorite> {
    return this.favoritesRepository.findOne({ userId, itemId });
  }

  async removeFavorite(userId: string, itemId: string): Promise<boolean> {
    const favorite = await this.favoritesRepository.findOneOrFail({
      where: {
        userId,
        itemId,
      },
    });
    await this.favoritesRepository.remove(favorite);
    return true;
  }
}
