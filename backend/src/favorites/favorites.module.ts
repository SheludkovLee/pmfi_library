import { Module } from '@nestjs/common';
import { FavoritesService } from './favorites.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FavoritesRepository } from './favorites.repository';
import { FavoritesMutationResolver } from './favorites.mutation-resolver';

@Module({
  imports: [TypeOrmModule.forFeature([FavoritesRepository])],
  providers: [FavoritesService, FavoritesMutationResolver],
  exports: [FavoritesService],
})
export class FavoritesModule {}
