import { Column, Entity, Tree, TreeChildren, TreeParent } from 'typeorm';
import { BaseEntity } from 'src/common';

const tableName = 'categories';

@Entity('categories')
@Tree('materialized-path')
export class Category extends BaseEntity {
  static tableName = tableName;

  @Column('text')
  name: string;

  @TreeParent({ onDelete: 'CASCADE' })
  parent: Category;

  @Column('text', { nullable: true })
  parentId: string;

  @TreeChildren()
  children: Category[];

  // @OneToMany(() => Item, (item) => item.category)
  // items: Item[];
}
