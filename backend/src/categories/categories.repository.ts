import { EntityRepository, TreeRepository } from 'typeorm';
import { Category } from './category.entity';

@EntityRepository(Category)
export class CategoriesRepository extends TreeRepository<Category> {}
