import { NestDataLoader } from '@pmfi/dataloader';
import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { CategoryModel } from './category.model';
import { CategoriesRepository } from './categories.repository';

@Injectable()
export class CategoryLoader implements NestDataLoader<string, CategoryModel> {
  constructor(private categoriesRepository: CategoriesRepository) {}

  generateDataLoader(): DataLoader<string, CategoryModel> {
    return new DataLoader<string, CategoryModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const categories = await this.categoriesRepository.findByIds(
        uniqueKeys as string[],
      );

      return keys.map(
        (key) =>
          categories.find((category) => category.id === key) ||
          new Error(`Could not load category with id: ${key}`),
      );
    });
  }
}
