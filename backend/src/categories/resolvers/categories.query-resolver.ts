import { Query, Resolver } from '@nestjs/graphql';
import { CategoryModel } from '../category.model';
import { CategoriesService } from '../categories.service';

@Resolver(() => CategoryModel)
export class CategoriesQueryResolver {
  constructor(private categoriesService: CategoriesService) {}

  @Query(() => [CategoryModel])
  async categories(): Promise<CategoryModel[]> {
    return this.categoriesService.findAll();
  }
}
