import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { CategoryModel } from '../category.model';
import { CategoriesService } from '../categories.service';
import { CreateCategoryInput } from '../create-category.input';
import { ParseUUIDPipe } from '@nestjs/common';

@Resolver(() => CategoryModel)
export class CategoriesMutationResolver {
  constructor(private categoriesService: CategoriesService) {}

  @Mutation(() => CategoryModel)
  async createCategory(
    @Args('createCategoryInput') input: CreateCategoryInput,
  ): Promise<CategoryModel> {
    return this.categoriesService.create(input);
  }

  @Mutation(() => Boolean)
  async removeCategory(
    @Args('id', ParseUUIDPipe) id: string,
  ): Promise<boolean> {
    return this.categoriesService.remove(id);
  }
}
