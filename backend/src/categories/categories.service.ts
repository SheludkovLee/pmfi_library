import { Injectable } from '@nestjs/common';
import { CategoriesRepository } from './categories.repository';
import { CreateCategoryInput } from './create-category.input';
import { Category } from './category.entity';
import { FindTreeOptions } from 'typeorm/find-options/FindTreeOptions';

@Injectable()
export class CategoriesService {
  constructor(private categoriesRepository: CategoriesRepository) {}

  async create(input: CreateCategoryInput): Promise<Category> {
    const parent = await this.categoriesRepository.findOne(input.parentId);
    return this.categoriesRepository.save({ ...input, parent: parent });
  }

  async findAll(options?: FindTreeOptions): Promise<Category[]> {
    return this.categoriesRepository.findTrees(options);
  }

  async findChildren(id: string): Promise<Category> {
    const parent = await this.categoriesRepository.findOneOrFail(id);
    return this.categoriesRepository.findDescendantsTree(parent);
  }

  async remove(id: string): Promise<boolean> {
    const category = await this.categoriesRepository.findOneOrFail(id);
    await this.categoriesRepository.remove(category);
    return true;
  }
}
