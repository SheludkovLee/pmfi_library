import { Module } from '@nestjs/common';
import { CategoriesQueryResolver } from './resolvers/categories.query-resolver';
import { CategoriesService } from './categories.service';
import { CategoriesMutationResolver } from './resolvers/categories.mutation-resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoriesRepository } from './categories.repository';

@Module({
  imports: [TypeOrmModule.forFeature([CategoriesRepository])],
  providers: [
    CategoriesMutationResolver,
    CategoriesQueryResolver,
    CategoriesService,
  ],
  exports: [CategoriesService],
})
export class CategoriesModule {}
