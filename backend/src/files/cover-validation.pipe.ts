import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { FileUpload } from 'graphql-upload';
import * as fs from 'fs';
import { createWriteStream, ReadStream } from 'fs';
import { v4 } from 'uuid';
import { fromStream } from 'file-type';

const VALID_EXTENSIONS = ['jpg', 'jpeg'];
const VALID_MIME_TYPES = ['image/jpeg'];
// const MAX_SIZE = 10 * 1024 * 1024;
const UPLOAD_PATH = './uploads/covers/';

@Injectable()
export class CoverValidationPipe implements PipeTransform {
  async transform(file?: FileUpload): Promise<string> {
    const { createReadStream, filename } = await file;
    if (!filename) {
      return null;
    }
    const isValidMime = await this.toValidateMime(createReadStream());
    if (!isValidMime) {
      throw new BadRequestException(
        `Недопустимое расширение обложки. Поддерживаются: ${VALID_EXTENSIONS}`,
      );
    }

    const newFilename = v4() + `.${filename}`;
    await fs.promises.mkdir(UPLOAD_PATH, { recursive: true });
    await new Promise((res) =>
      createReadStream().pipe(
        createWriteStream(UPLOAD_PATH + newFilename).on('close', res),
      ),
    );

    return newFilename;
  }

  async toValidateMime(stream: ReadStream): Promise<boolean> {
    try {
      const { ext, mime } = await fromStream(stream);

      if (!VALID_EXTENSIONS.includes(ext) && !VALID_MIME_TYPES.includes(mime)) {
        return false;
      }
    } catch (e) {
      return false;
    }
    return true;
  }
}
