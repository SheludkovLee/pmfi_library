import { Injectable } from '@nestjs/common';
import { createCanvas } from 'canvas';

@Injectable()
export class FilesService {
  async generateDefaultCover(
    title: string,
    authorName: string,
  ): Promise<string> {
    const width = 40;
    const height = 60;

    const canvas = createCanvas(width, height);

    const context = canvas.getContext('2d');
    context.fillRect(0, 0, width, height);
    return 'path';
  }
}
