import { IPaginationMeta } from 'nestjs-typeorm-paginate/dist/interfaces';
import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType('PaginationMeta', { description: 'Мета информациия пагинации' })
export class PaginationMetaModel implements IPaginationMeta {
  @Field(() => Int)
  itemCount: number;
  /**
   * the total amount of items
   */
  @Field(() => Int, { nullable: true })
  totalItems?: number;
  /**
   * the amount of items that were requested per page
   */
  @Field(() => Int)
  itemsPerPage: number;
  /**
   * the total amount of pages in this paginator
   */
  @Field(() => Int, { nullable: true })
  totalPages?: number;
  /**
   * the current page this paginator "points" to
   */
  @Field(() => Int)
  currentPage: number;
}
