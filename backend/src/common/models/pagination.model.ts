import { Field, ObjectType } from '@nestjs/graphql';
import { PaginationMetaModel } from './pagination-meta.model';

@ObjectType('PaginationModel', {
  isAbstract: true,
  description: 'Модель пагинации',
})
export class PaginationModel {
  @Field(() => PaginationMetaModel)
  meta!: PaginationMetaModel;
}
