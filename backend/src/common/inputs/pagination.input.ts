import { Field, InputType, Int } from '@nestjs/graphql';

@InputType({ description: 'Инпут на пагинацию' })
export class PaginationInput {
  @Field(() => Int, { defaultValue: 10 })
  limit: number;

  @Field(() => Int, { defaultValue: 1 })
  page: number;
}
