import { Field, InputType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';
import { Order, SortFields } from '../enums';

@InputType({ description: 'Инпут на сортировку' })
export class SortInput {
  @Field(() => SortFields, { defaultValue: SortFields.CREATED_AT })
  @IsOptional()
  sort: SortFields;

  @Field(() => Order, { nullable: true })
  @IsOptional()
  order: Order | null;
}
