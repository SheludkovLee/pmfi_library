import { Field, ID, InputType } from '@nestjs/graphql';
import { ItemType } from '../../items/item-type.enum';

@InputType({ description: 'Инпут для фильтрации' })
export class FilterInput {
  @Field({ defaultValue: false, description: 'Отфильтровать по избранным' })
  isFavorite?: boolean;

  @Field({
    defaultValue: false,
    description: 'Отфильтровать по своим ресурсам',
  })
  isOwned?: boolean;

  @Field({ nullable: true, description: 'Отф' })
  isElectronic?: boolean;

  @Field(() => ItemType, { nullable: true })
  type?: ItemType | null;

  @Field(() => ID, { nullable: true })
  categoryId?: string;
}
