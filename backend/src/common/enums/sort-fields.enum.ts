import { registerEnumType } from '@nestjs/graphql';

export enum SortFields {
  CREATED_AT = '"createdAt"',
  AVERAGE_RATING = 'averageRating',
  COUNT_COMMENTS = 'countComments',
}

registerEnumType(SortFields, {
  name: 'SortFields',
});
