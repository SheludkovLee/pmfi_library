import { registerEnumType } from '@nestjs/graphql';

export enum Order {
  ASCENDING = 'ASC',
  DESCENDING = 'DESC',
}

registerEnumType(Order, {
  name: 'Order',
});
