import { CanActivate, ExecutionContext } from '@nestjs/common';
import { EntityTarget, getRepository } from 'typeorm';
import { GqlExecutionContext } from '@nestjs/graphql';
import { User } from 'src/users/user.entity';

export abstract class CanAccessGuard<Entity extends { userId: string }>
  implements CanActivate
{
  protected constructor(
    private entity: EntityTarget<Entity>,
    private idParam: string = 'id',
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const ctx = GqlExecutionContext.create(context);
    const { req } = ctx.getContext();
    const { user, body }: { user: User; body: any } = req;

    if (!user || !body) {
      return false;
    }

    if (user.isAdmin) {
      return true;
    }

    const authUserId: string = user.id;
    const idSubject = this.getEntityId(req);

    const repo = getRepository(this.entity);

    const subject = await repo.findOne(idSubject);

    if (!subject) {
      return true;
    }

    return subject.userId === authUserId;
  }

  protected getEntityId(req: any): number {
    const { body } = req;
    return body.variables[this.idParam];
  }
}
