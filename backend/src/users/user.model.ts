import { Field, ObjectType } from '@nestjs/graphql';
import { BaseModel } from 'src/common';

@ObjectType('User', {
  description: 'Модель пользователя',
})
export class UserModel extends BaseModel {
  @Field({ description: 'Имя пользователя для входа' })
  username: string;

  @Field({ nullable: true, description: 'E-mail пользователя' })
  email?: string;

  @Field({ defaultValue: false, description: 'Расширенные права пользователя' })
  isAdmin: boolean;

  @Field({
    nullable: true,
    description:
      'Ссылка на соцсеть пользователя (для возможности связаться по поводу буккроссинга)',
  })
  socialMedia?: string;
}
