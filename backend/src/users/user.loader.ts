import { NestDataLoader } from '@pmfi/dataloader';
import { UserModel } from './user.model';
import * as DataLoader from 'dataloader';
import { Injectable } from '@nestjs/common';
import { UsersRepository } from './users.repository';

@Injectable()
export class UserLoader implements NestDataLoader<string, UserModel> {
  constructor(private usersRepository: UsersRepository) {}

  generateDataLoader(): DataLoader<string, UserModel> {
    return new DataLoader<string, UserModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const users = await this.usersRepository.findByIds(
        uniqueKeys as string[],
      );

      return keys.map(
        (key) =>
          users.find((user) => user.id === key) ||
          new Error(`Could not load user with id: ${key}`),
      );
    });
  }
}
