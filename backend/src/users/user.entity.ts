import { Column, Entity, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';
import { Item } from 'src/items/entities/item.entity';
import { Comment } from 'src/comments/comment.entity';
import { Favorite } from 'src/favorites/favorite.entity';
import { Rating } from 'src/ratings/rating.entity';
import { BaseEntity } from 'src/common';

const tableName = 'users';

@Entity(tableName)
export class User extends BaseEntity {
  static tableName = tableName;

  @Column('varchar', { unique: true, length: 50 })
  username: string;

  @Exclude({ toPlainOnly: true })
  @Column('varchar', { length: 60 })
  password: string;

  @Column('boolean', { default: false })
  isAdmin: boolean;

  @Column('varchar', { nullable: true, unique: true, length: 50 })
  email: string;

  @Column('varchar', { nullable: true, length: 150 })
  socialMedia: string;

  @OneToMany(() => Item, (item) => item.user)
  items: Item[];

  @OneToMany(() => Comment, (comment) => comment.user)
  comments: Comment[];

  @OneToMany(() => Favorite, (favorite) => favorite.user)
  favorites: Favorite[];

  @OneToMany(() => Rating, (rating) => rating.user)
  ratings: Rating[];
}
