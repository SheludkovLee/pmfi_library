import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class HashService {
  async generateSalt(): Promise<string> {
    return bcrypt.genSalt();
  }

  async generateHash(password: string): Promise<string> {
    const salt = await this.generateSalt();
    return bcrypt.hash(password, salt);
  }

  async compareHash(password: string, hash: string): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }
}
