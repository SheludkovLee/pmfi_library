import { Injectable } from '@nestjs/common';
import { User } from '../user.entity';
import { HashService } from './hash.service';
import { FindOneOptions } from 'typeorm';
import { SignUpInput } from '../../auth/inputs/sign-up.input';
import { UpdateUserInput } from '../inputs/update-user.input';
import { UsersRepository } from '../users.repository';

@Injectable()
export class UsersService {
  constructor(
    private usersRepository: UsersRepository,
    private hashService: HashService,
  ) {}

  async createOne(input: SignUpInput): Promise<User> {
    input.password = await this.hashService.generateHash(input.password);
    return this.usersRepository.save(input);
  }

  async findOneByOptions(options: FindOneOptions<User>): Promise<User | null> {
    return this.usersRepository.findOne(options);
  }

  async findOneOrFail(id: string): Promise<User> {
    return this.usersRepository.findOneOrFail(id);
  }

  async update(id: string, input: UpdateUserInput): Promise<User | null> {
    if (input.password) {
      input.password = await this.hashService.generateHash(input.password);
    }
    await this.usersRepository.update(id, input);
    return this.findOneOrFail(id);
  }

  async remove(id: string): Promise<boolean> {
    const user = await this.findOneOrFail(id);
    await this.usersRepository.remove(user);
    return true;
  }
}
