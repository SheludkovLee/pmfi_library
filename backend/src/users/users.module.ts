import { Module } from '@nestjs/common';
import { HashService } from './services/hash.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersRepository } from './users.repository';
import { UsersService } from './services/users.service';
import { UsersResolver } from './resolvers/users.resolver';
import { UsersMutationResolver } from './resolvers/users.mutation-resolver';
import { ItemsModule } from '../items/items.module';
import { UsersQueryResolver } from './resolvers/users.query.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([UsersRepository]), ItemsModule],
  providers: [
    UsersService,
    HashService,
    UsersResolver,
    UsersMutationResolver,
    UsersQueryResolver,
  ],
  exports: [UsersService, HashService],
})
export class UsersModule {}
