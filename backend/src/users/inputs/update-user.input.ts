import { Field, InputType, PartialType } from '@nestjs/graphql';
import { SignUpInput } from '../../auth/inputs/sign-up.input';
import { IsEmail, IsOptional, IsUrl, Length } from 'class-validator';
import { Transform } from 'class-transformer';

@InputType({ description: 'Инпут на обновление пользователя' })
export class UpdateUserInput extends PartialType(SignUpInput) {
  @Field({ description: 'E-mail пользователя' })
  @IsOptional()
  @IsEmail(null, { message: 'Некорректный e-mail' })
  @Length(3, 50, { message: 'Недопустимая длина e-mail' })
  @Transform(({ value }) => value.trim().toLowerCase())
  email: string;

  @Field({ description: 'Соц.Сеть пользователя' })
  @IsOptional()
  @IsUrl(null, { message: 'Неккоректная ссылка' })
  @Length(3, 150, { message: 'Недопустимая длина ссылки' })
  @Transform(({ value }) => value.trim())
  socialMedia: string;
}
