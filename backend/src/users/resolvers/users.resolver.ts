import { Args, Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { UserModel } from '../user.model';
import { UsersService } from '../services/users.service';
import { ItemsService } from 'src/items/services/items.service';
import { PaginationItemsModel } from 'src/items/models/pagination.items.model';
import { PaginationInput, SortInput } from 'src/common';

@Resolver(() => UserModel)
export class UsersResolver {
  constructor(
    private usersService: UsersService,
    private itemsService: ItemsService,
  ) {}

  @ResolveField(() => PaginationItemsModel, {
    description: 'Ресурсы, загруженные пользователем',
  })
  async selfItems(
    @Parent() user: UserModel,
    @Args('paginationInput') pagination: PaginationInput,
    @Args('sortInput') sort: SortInput,
  ): Promise<PaginationItemsModel> {
    const userId = user.id;
    return this.itemsService.findAll(
      pagination,
      sort,
      { isOwned: true },
      userId,
    );
  }

  @ResolveField(() => PaginationItemsModel, {
    description: 'Ресурсы, добавленные пользователем в изрбранное',
  })
  async favorites(
    @Parent() user: UserModel,
    @Args('paginationInput') pagination: PaginationInput,
    @Args('sortInput') sort: SortInput,
  ) {
    const userId = user.id;
    return this.itemsService.findAll(
      pagination,
      sort,
      { isFavorite: true },
      userId,
    );
  }
}
