import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { UserModel } from '../user.model';
import { UsersService } from '../services/users.service';
import { UpdateUserInput } from '../inputs/update-user.input';
import { Self } from 'src/common/decorators/self.decorator';

@Resolver(() => UserModel)
export class UsersMutationResolver {
  constructor(private usersService: UsersService) {}

  @Mutation(() => UserModel, { description: 'Обновить данные пользователя' })
  async updateUser(
    @Self('id') userId: string,
    @Args('updateUserInput') input: UpdateUserInput,
  ): Promise<UserModel> {
    return this.usersService.update(userId, input);
  }
}
