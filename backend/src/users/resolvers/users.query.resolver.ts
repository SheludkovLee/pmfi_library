import { Query, Resolver } from '@nestjs/graphql';
import { UserModel } from '../user.model';
import { UsersService } from '../services/users.service';
import { Self } from '../../common';

@Resolver(() => UserModel)
export class UsersQueryResolver {
  constructor(private usersService: UsersService) {}

  @Query(() => UserModel, {
    description: 'Найти пользователя по ID, иначе: NotFoundException',
  })
  async self(@Self('id') id: string): Promise<UserModel> {
    return this.usersService.findOneOrFail(id);
  }
}
