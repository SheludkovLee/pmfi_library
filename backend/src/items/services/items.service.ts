import { ConflictException, Injectable } from '@nestjs/common';
import { ItemsRepository } from '../repositories/items.repository';
import { Item } from '../entities/item.entity';
import { CreateItemInput } from '../inputs/create-item.input';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { UpdateItemInput } from '../inputs/update-item.input';
import { AuthorsRepository } from '../repositories/authors.repository';
import { PublishersRepository } from '../repositories/publishers.repository';
import { CategoriesService } from 'src/categories/categories.service';
import { FilterInput, SortInput } from 'src/common';
import { FilesService } from '../../files/files.service';

@Injectable()
export class ItemsService {
  constructor(
    private itemsRepository: ItemsRepository,
    private authorsRepository: AuthorsRepository,
    private publishersRepository: PublishersRepository,
    private categoriesService: CategoriesService,
    private filesService: FilesService,
  ) {}

  async create(
    userId: string,
    input: CreateItemInput,
    coverName: string,
  ): Promise<Item> {
    const author = await this.authorsRepository.findOrCreate(input.author);
    const publisher = await this.publishersRepository.findOrCreate(
      input.publisher,
    );
    const existItem = await this.itemsRepository.findByUniqueFields(
      input.title,
      author.id,
      true,
    );

    if (!coverName) {
      coverName = await this.filesService.generateDefaultCover(
        input.title,
        author.name,
      );
    }

    if (existItem) {
      throw new ConflictException('Такой ресурс уже существует');
    }

    return this.itemsRepository.save({
      ...input,
      userId,
      author,
      publisher,
      coverName,
      isElectronic: true,
    });
  }

  async findOne(id: string): Promise<Item> {
    return this.itemsRepository.findOneOrFail(id);
  }

  async kursach_find_all() {
    return this.itemsRepository.find();
  }

  async findAll(
    paginationOptions: IPaginationOptions,
    { sort, order }: SortInput,
    filterOptions?: FilterInput,
    userId?: string,
  ): Promise<Pagination<Item>> {
    const qb = this.itemsRepository.createQueryBuilder('items');
    qb.orderBy(sort, order);

    if (filterOptions?.isOwned) {
      qb.andWhere({ userId: userId });
    }

    if (filterOptions?.isFavorite) {
      qb.innerJoin('items.favorites', 'favorites').andWhere(
        'favorites.userId = :userId',
        { userId: userId },
      );
    }

    if (filterOptions?.type) {
      qb.andWhere({ type: filterOptions.type });
    }

    if (filterOptions.isElectronic !== null) {
      qb.andWhere({ isElectronic: filterOptions.isElectronic });
    }

    if (filterOptions.categoryId) {
      qb.andWhere({ categoryId: filterOptions.categoryId });
      const categories = await this.categoriesService.findChildren(
        filterOptions.categoryId,
      );
      categories.children.forEach((category) =>
        qb.andWhere({ categoryId: category.id }),
      );
    }

    return paginate<Item>(qb, paginationOptions);
  }

  async update(
    id: string,
    input: UpdateItemInput,
    coverName: string,
  ): Promise<Item> {
    const item = await this.findOne(id);

    const author = await this.authorsRepository.findOrCreate(input.author);
    const publisher = await this.publishersRepository.findOrCreate(
      input.publisher,
    );

    await this.itemsRepository.update(item.id, {
      ...input,
      author,
      publisher,
      coverName,
    });
    return this.findOne(id);
  }

  async remove(id: string): Promise<boolean> {
    const item = await this.findOne(id);
    await this.itemsRepository.remove(item);
    return true;
  }
}
