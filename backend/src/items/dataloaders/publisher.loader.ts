import { Injectable } from '@nestjs/common';
import { NestDataLoader } from '@pmfi/dataloader';
import * as DataLoader from 'dataloader';
import { PublisherModel } from '../models/publisher.model';
import { PublishersRepository } from '../repositories/publishers.repository';

@Injectable()
export class PublisherLoader implements NestDataLoader<string, PublisherModel> {
  constructor(private publishersRepository: PublishersRepository) {}

  generateDataLoader(): DataLoader<string, PublisherModel> {
    return new DataLoader<string, PublisherModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const publishers = await this.publishersRepository.findByIds(
        uniqueKeys as string[],
      );

      return keys.map(
        (key) =>
          publishers.find((publisher) => publisher.id === key) ||
          new Error(`Could not load publisher with id: ${key}`),
      );
    });
  }
}
