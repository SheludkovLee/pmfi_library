import { Injectable } from '@nestjs/common';
import { NestDataLoader } from '@pmfi/dataloader';
import * as DataLoader from 'dataloader';
import { AuthorsRepository } from '../repositories/authors.repository';
import { AuthorModel } from '../models/author.model';

@Injectable()
export class AuthorLoader implements NestDataLoader<string, AuthorModel> {
  constructor(private authorsRepository: AuthorsRepository) {}

  generateDataLoader(): DataLoader<string, AuthorModel> {
    return new DataLoader<string, AuthorModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const authors = await this.authorsRepository.findByIds(
        uniqueKeys as string[],
      );

      return keys.map(
        (key) =>
          authors.find((author) => author.id === key) ||
          new Error(`Could not load author with id: ${key}`),
      );
    });
  }
}
