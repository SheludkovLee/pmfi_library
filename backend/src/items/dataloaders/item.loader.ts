import { Injectable } from '@nestjs/common';
import { ItemsRepository } from '../repositories/items.repository';
import { ItemModel } from '../models/item.model';
import { NestDataLoader } from '@pmfi/dataloader';
import * as DataLoader from 'dataloader';

@Injectable()
export class ItemLoader implements NestDataLoader<string, ItemModel> {
  constructor(private itemsRepository: ItemsRepository) {}

  generateDataLoader(): DataLoader<string, ItemModel> {
    return new DataLoader<string, ItemModel>(async (keys) => {
      const uniqueKeys = [...new Set(keys)];
      const items = await this.itemsRepository.findByIds(
        uniqueKeys as string[],
      );

      return keys.map(
        (key) =>
          items.find((item) => item.id === key) ||
          new Error(`Could not load item with id: ${key}`),
      );
    });
  }
}
