import { BaseModel } from 'src/common';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { ItemType } from '../item-type.enum';

@ObjectType('Item', {
  description: `Модель ресурса`,
})
export class ItemModel extends BaseModel {
  @Field({ description: 'Электронный ресурс?' })
  isElectronic: boolean;

  @Field(() => ItemType, {
    description: `Тип ресурса. Типы заданы статически: ${ItemType} `,
  })
  type: ItemType;

  @Field({ description: 'Название ресурса' })
  title: string;

  @Field({ description: 'Ссылка на обложку ресурса' })
  coverName: string;

  @Field({ nullable: true, description: 'Описание/аннотация ресурса' })
  description?: string;

  @Field({ nullable: true, description: 'Год публикации/издания ресурса' })
  publicationYear?: number;

  @Field({
    nullable: true,
    description: 'ISBN ресурса. Уникальный идентификатор книги',
  })
  ISBN?: string;

  @Field({ nullable: true, description: 'Ссылка на электронный ресурс' })
  source?: string;

  @Field(() => ID, { description: 'ID пользователя, который загрузил ресурс' })
  userId: string;

  @Field(() => ID, { description: 'ID автора ресурса' })
  authorId: string;
  //
  // @Field(() => ID, { description: 'ID категории ресурса' })
  // categoryId: string;

  @Field(() => ID, {
    nullable: true,
    description: 'ID издателя/издательства ресурса',
  })
  publisherId?: string;
}
