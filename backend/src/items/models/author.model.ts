import { BaseModel } from 'src/common';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType('Author', {
  description: 'Модель автора ресурса',
})
export class AuthorModel extends BaseModel {
  @Field({ description: 'Имя автора' })
  name: string;
}
