import { BaseModel } from 'src/common';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType('Publisher', {
  description: 'Модель издателя/издательства ресурса',
})
export class PublisherModel extends BaseModel {
  @Field({ description: 'Имя/название издателя/издательства' })
  name: string;
}
