import { Field, ObjectType } from '@nestjs/graphql';
import { PaginationModel } from 'src/common';
import { ItemModel } from './item.model';

@ObjectType('PaginationItems', {
  description: 'Модель пагинированных ресурсов',
})
export class PaginationItemsModel extends PaginationModel {
  @Field(() => [ItemModel])
  items: ItemModel[];
}
