import { registerEnumType } from '@nestjs/graphql';

export enum ItemType {
  BOOK = 'BOOK',
  ARTICLE = 'ARTICLE',
  MANUAL = 'MANUAL',
  MEDIA = 'MEDIA',
}

registerEnumType(ItemType, {
  name: 'ItemType',
  description: 'Поддерживаемые типы литературы',
});
