import { CanAccessGuard } from '../common/guards/can-access.guard';
import { Item } from './entities/item.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CanAccessItemGuard extends CanAccessGuard<Item> {
  constructor() {
    super(Item);
  }
}
