import { EntityRepository, Repository } from 'typeorm';
import { Author } from '../entities/author.entity';

@EntityRepository(Author)
export class AuthorsRepository extends Repository<Author> {
  async findOrCreate(name: string): Promise<Author | null> {
    const author = await this.createQueryBuilder('authors')
      .where('LOWER(authors.name) = LOWER(:name)', { name })
      .getOne();
    return author ? author : this.save({ name });
  }
}
