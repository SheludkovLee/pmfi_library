import { EntityRepository, Repository } from 'typeorm';
import { Item } from '../entities/item.entity';

@EntityRepository(Item)
export class ItemsRepository extends Repository<Item> {
  findByUniqueFields(
    title: string,
    authorId: string,
    isElectronic: boolean,
  ): Promise<Item> {
    return this.createQueryBuilder('items')
      .where('LOWER(items.title) = LOWER(:title)', { title })
      .andWhere('items.authorId = :authorId', { authorId })
      .andWhere('items.isElectronic = :isElectronic', { isElectronic })
      .getOne();
  }
}
