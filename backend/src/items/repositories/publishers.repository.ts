import { EntityRepository, Repository } from 'typeorm';
import { Publisher } from '../entities/publisher.entity';

@EntityRepository(Publisher)
export class PublishersRepository extends Repository<Publisher> {
  async findOrCreate(name: string): Promise<Publisher | null> {
    if (!name) {
      return null;
    }
    const publisher = await this.createQueryBuilder('publishers')
      .where('LOWER(publishers.name) = LOWER(:name)', { name })
      .getOne();
    return publisher ? publisher : this.save({ name });
  }
}
