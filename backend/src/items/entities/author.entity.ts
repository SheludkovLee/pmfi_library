import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from 'src/common';
import { Item } from './item.entity';

const tableName = 'authors';

@Entity(tableName, { orderBy: { name: 'ASC' } })
export class Author extends BaseEntity {
  static tableName = tableName;

  @Column('varchar', { unique: true, length: 50 })
  name: string;

  @OneToMany(() => Item, (item) => item.author)
  items: Item[];
}
