import { BaseEntity } from 'src/common';
import { Column, Entity, OneToMany } from 'typeorm';
import { Item } from './item.entity';

const tableName = 'publishers';

@Entity(tableName, { orderBy: { name: 'ASC' } })
export class Publisher extends BaseEntity {
  static tableName = tableName;

  @Column('varchar', { unique: true })
  name: string;

  @OneToMany(() => Item, (item) => item.publisher)
  items: Item[];
}
