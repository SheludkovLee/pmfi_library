import { BaseEntity } from 'src/common';
import { Column, Entity, ManyToOne, OneToMany, Unique } from 'typeorm';
import { User } from 'src/users/user.entity';
import { Comment } from 'src/comments/comment.entity';
import { Favorite } from 'src/favorites/favorite.entity';
import { Rating } from 'src/ratings/rating.entity';
import { Author } from './author.entity';
import { Publisher } from './publisher.entity';
import { ItemType } from '../item-type.enum';

const tableName = 'items';

@Entity(tableName)
@Unique(['title', 'author', 'isElectronic'])
export class Item extends BaseEntity {
  static tableName = tableName;

  @Column('boolean', { default: false })
  isElectronic: boolean;

  @Column('enum', { enum: ItemType })
  type: ItemType;

  @Column('varchar', { length: 50 })
  title: string;

  // TODO default cover generator
  @Column('varchar', { default: 'default.jpg' })
  coverName: string;

  @Column('varchar', { nullable: true, length: 350 })
  description: string;

  @Column('int2', { nullable: true })
  publicationYear: number;

  @Column('varchar', { nullable: true, unique: true, length: 15 })
  ISBN: string;

  @Column('varchar', { nullable: true, length: 150 })
  source: string;

  @Column('uuid')
  userId: string;

  @Column('uuid')
  authorId: string;

  // @Column('uuid')
  // categoryId: string;

  @Column('uuid', { nullable: true })
  publisherId: string;

  @ManyToOne(() => User, (user) => user.items, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  user: User;

  @ManyToOne(() => Author, (author) => author.items, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  author: Author;

  // @ManyToOne(() => Category, (category) => category.items, {
  //   cascade: true,
  //   onDelete: 'CASCADE',
  // })
  // category: Category;

  @ManyToOne(() => Publisher, (publisher) => publisher.items, {
    cascade: true,
  })
  publisher: Publisher;

  @OneToMany(() => Comment, (comment) => comment.item)
  comments: Comment[];

  @OneToMany(() => Favorite, (favorite) => favorite.item)
  favorites: Favorite[];

  @OneToMany(() => Rating, (rating) => rating.item)
  ratings: Rating[];
}
