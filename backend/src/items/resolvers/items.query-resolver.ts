import { Args, Query, Resolver } from '@nestjs/graphql';
import { ItemModel } from '../models/item.model';
import { ItemsService } from '../services/items.service';

@Resolver(() => ItemModel)
export class ItemsQueryResolver {
  constructor(private itemsService: ItemsService) {}

  @Query(() => ItemModel)
  async findItem(@Args('id') id: string): Promise<ItemModel> {
    return this.itemsService.findOne(id);
  }

  @Query(() => [ItemModel])
  async findItems(): // @Args('paginationInput') pagination: PaginationInput,
  // @Args('sortInput') sort?: SortInput,
  // @Args('filterInput') filter?: FilterInput,
  Promise<ItemModel[]> {
    // return this.itemsService.findAll(pagination, sort, filter);
    return this.itemsService.kursach_find_all();
  }
}
