import { Args, Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { ItemModel } from '../models/item.model';
import { Loader } from '@pmfi/dataloader';
import { UserLoader } from 'src/users/user.loader';
import DataLoader from 'dataloader';
import { UserModel } from 'src/users/user.model';
import { CommentsService } from 'src/comments/comments.service';
import { AuthorLoader } from '../dataloaders/author.loader';
import { AuthorModel } from '../models/author.model';
import { PublisherLoader } from '../dataloaders/publisher.loader';
import { PublisherModel } from '../models/publisher.model';
import { FavoritesService } from 'src/favorites/favorites.service';
import { RatingsService } from 'src/ratings/ratings.service';
import { PaginationCommentsModel } from 'src/comments/models/pagination-comments.model';
import { PaginationInput, Self, SortInput } from 'src/common';

@Resolver(() => ItemModel)
export class ItemsResolver {
  constructor(
    private commentsService: CommentsService,
    private favoritesService: FavoritesService,
    private ratingsService: RatingsService,
  ) {}

  @ResolveField(() => UserModel)
  async user(
    @Parent() item: ItemModel,
    @Loader(UserLoader) userLoader: DataLoader<string, UserModel>,
  ): Promise<UserModel> {
    const { userId } = item;
    return userLoader.load(userId);
  }

  @ResolveField(() => AuthorModel)
  async author(
    @Parent() item: ItemModel,
    @Loader(AuthorLoader) authorLoader: DataLoader<string, AuthorModel>,
  ): Promise<AuthorModel> {
    const { authorId } = item;
    return authorLoader.load(authorId);
  }

  @ResolveField(() => PublisherModel, { nullable: true })
  async publisher(
    @Parent() item: ItemModel,
    @Loader(PublisherLoader)
    publisherLoader: DataLoader<string, PublisherModel>,
  ): Promise<PublisherModel> {
    const { publisherId } = item;
    return publisherId ? publisherLoader.load(publisherId) : null;
  }

  // @ResolveField(() => CategoryModel)
  // async category(
  //   @Parent() item: ItemModel,
  //   @Loader(CategoryLoader) categoryLoader: DataLoader<string, CategoryModel>,
  // ): Promise<CategoryModel> {
  //   const { categoryId } = item;
  //   return categoryLoader.load(categoryId);
  // }

  @ResolveField(() => PaginationCommentsModel)
  async comments(
    @Parent() item: ItemModel,
    @Args('paginationInput') pagination: PaginationInput,
    @Args('sortInput') sort: SortInput,
  ): Promise<PaginationCommentsModel> {
    const itemId = item.id;
    return this.commentsService.findAll(itemId, pagination, sort);
  }

  @ResolveField(() => Boolean)
  async isFavorite(
    @Parent() item: ItemModel,
    @Self('id') userId: string,
  ): Promise<boolean> {
    const itemId = item.id;
    const favorite = await this.favoritesService.findFavorite(userId, itemId);
    return !!favorite;
  }

  @ResolveField(() => Number, { nullable: true })
  async averageRating(@Parent() item: ItemModel): Promise<number> {
    const itemId = item.id;
    return this.ratingsService.calculateAverageRating(itemId);
  }

  @ResolveField(() => Number, { nullable: true })
  async selfRatingValue(
    @Parent() item: ItemModel,
    @Self('id') userId: string,
  ): Promise<number | null> {
    const itemId = item.id;
    const rating = await this.ratingsService.findOne(userId, itemId);
    return rating?.value;
  }
}
