import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { ItemModel } from '../models/item.model';
import { ItemsService } from '../services/items.service';
import { Self } from 'src/common';
import { CreateItemInput } from '../inputs/create-item.input';
import { UpdateItemInput } from '../inputs/update-item.input';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { CanAccessItemGuard } from '../can-access-item.guard';
import { GraphQLUpload } from 'graphql-upload';
import { CoverValidationPipe } from 'src/files/cover-validation.pipe';

@Resolver(() => ItemModel)
export class ItemsMutationResolver {
  constructor(private itemsService: ItemsService) {}

  @Mutation(() => Boolean)
  async uploadFile(
    @Args(
      { name: 'cover', type: () => GraphQLUpload, nullable: true },
      CoverValidationPipe,
    )
    coverName?: string,
  ): Promise<boolean> {
    console.log(coverName);
    return true;
  }

  @Mutation(() => ItemModel)
  async createItem(
    @Self('id') userId: string,
    @Args('createItemInput') input: CreateItemInput,
    // @Args('cover', { type: () => GraphQLUpload }, CoverValidationPipe)
    // coverName?: string,
  ): Promise<ItemModel> {
    const coverName = 'cover.jpg';
    console.log('lol');
    return this.itemsService.create(userId, input, coverName);
  }

  @Mutation(() => ItemModel)
  @UseGuards(CanAccessItemGuard)
  async updateItem(
    @Args('id', ParseUUIDPipe) id: string,
    @Args('updateItemInput') input: UpdateItemInput,
    @Args('cover', { type: () => GraphQLUpload }, CoverValidationPipe)
    coverName?: string,
  ): Promise<ItemModel> {
    return this.itemsService.update(id, input, coverName);
  }

  @Mutation(() => Boolean)
  @UseGuards(CanAccessItemGuard)
  async removeItem(@Args('id', ParseUUIDPipe) id: string): Promise<boolean> {
    return this.itemsService.remove(id);
  }
}
