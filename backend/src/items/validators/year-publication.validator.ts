import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'YearPublicationValidator', async: false })
export class YearPublicationValidator implements ValidatorConstraintInterface {
  validate(year: number) {
    return year >= 1900 && year <= new Date().getFullYear();
  }

  defaultMessage() {
    return `Недопустимая дата публикации`;
  }
}
