import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'IsbnValidator', async: false })
export class IsbnValidator implements ValidatorConstraintInterface {
  validate(isbn: string): boolean {
    return isbn.length >= 10 || isbn.length <= 13;
  }

  defaultMessage(): string {
    return 'Недопустимый формат ISBN';
  }
}
