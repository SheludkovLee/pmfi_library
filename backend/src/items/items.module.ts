import { Module } from '@nestjs/common';
import { ItemsService } from './services/items.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ItemsRepository } from './repositories/items.repository';
import { ItemsQueryResolver } from './resolvers/items.query-resolver';
import { ItemsMutationResolver } from './resolvers/items.mutation-resolver';
import { UserLoader } from 'src/users/user.loader';
import { CommentsModule } from 'src/comments/comments.module';
import { UsersRepository } from 'src/users/users.repository';
import { AuthorsRepository } from './repositories/authors.repository';
import { PublishersRepository } from './repositories/publishers.repository';
import { ItemsResolver } from './resolvers/items.resolver';
import { FavoritesModule } from 'src/favorites/favorites.module';
import { RatingsModule } from 'src/ratings/ratings.module';
import { AuthorLoader } from './dataloaders/author.loader';
import { PublisherLoader } from './dataloaders/publisher.loader';
import { CategoryLoader } from 'src/categories/category.loader';
import { CategoriesModule } from 'src/categories/categories.module';
import { CategoriesRepository } from 'src/categories/categories.repository';
import { FilesModule } from 'src/files/files.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ItemsRepository,
      AuthorsRepository,
      PublishersRepository,
      UsersRepository,
      CategoriesRepository,
    ]),
    CommentsModule,
    FavoritesModule,
    RatingsModule,
    CategoriesModule,
    FilesModule,
  ],
  providers: [
    ItemsService,
    ItemsResolver,
    ItemsQueryResolver,
    ItemsMutationResolver,
    UserLoader,
    AuthorLoader,
    PublisherLoader,
    CategoryLoader,
  ],
  exports: [ItemsService],
})
export class ItemsModule {}
