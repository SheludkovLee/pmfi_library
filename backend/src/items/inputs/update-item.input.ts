import { InputType, PartialType } from '@nestjs/graphql';
import { CreateItemInput } from './create-item.input';

@InputType({ description: 'Инпут на обновление ресурса' })
export class UpdateItemInput extends PartialType(CreateItemInput) {}
