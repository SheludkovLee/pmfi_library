import {
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
  IsUrl,
  MaxLength,
  Validate,
  ValidateIf,
} from 'class-validator';
import { Field, InputType, Int } from '@nestjs/graphql';
import { ItemType } from '../item-type.enum';
import { Transform } from 'class-transformer';
import { YearPublicationValidator } from '../validators/year-publication.validator';
import { Item } from '../entities/item.entity';
import { IsbnValidator } from '../validators/isbn.validator';

@InputType({ description: 'Инпут на создание ресурса' })
export class CreateItemInput {
  @Field(() => String, {
    description: 'Эллектронный ресурс?',
    defaultValue: true,
  })
  @Transform(({ value }) => {
    console.log('IS_BOOLEAN ', value);
    return value === 'true';
  })
  isElectronic: boolean;

  @Field(() => ItemType, {
    description: `Тип ресурса. Типы заданы статически: ${ItemType}`,
  })
  @IsEnum(ItemType)
  type: ItemType;

  @Field({ description: 'Название ресурса' })
  @IsNotEmpty({ message: 'Название ресурса не может быть пустым' })
  @IsString()
  @MaxLength(50, { message: 'Недопустимая длина для названия ресурса' })
  @Transform(({ value }) => value.trim())
  // TODO validate lower case title
  title: string;

  // @Field({ description: 'ID категории ресурса' })
  // @IsNotEmpty({ message: 'Выбор категории обязателен для ресурса' })
  // @IsUUID()
  // categoryId: string;

  @Field({ description: 'Имя автора ресурса' })
  @IsNotEmpty({ message: 'Выбор автора обазателен для ресурса' })
  @IsString()
  @MaxLength(50, { message: 'Недопустимая длина для автора ресурса' })
  @Transform(({ value }) => value.trim())
  author: string;

  @Field({ nullable: true, description: 'Издатель/издательство ресурса' })
  @IsOptional()
  @IsString()
  @MaxLength(50, {
    message: 'Недопустимая длина для издателя/издательства ресурса',
  })
  @Transform(({ value }) => value.trim())
  publisher?: string;

  @Field({ nullable: true, description: 'Описание/аннотация ресурса' })
  @IsOptional()
  @IsString()
  @MaxLength(50, { message: 'Недопустимая длина для описания' })
  @Transform(({ value }) => value.trim())
  description?: string;

  @Field(() => Int, {
    nullable: true,
    description: 'Год публикации/издания ресурса',
  })
  @IsOptional()
  @IsPositive()
  @Validate(YearPublicationValidator)
  publicationYear?: number;

  @Field({
    nullable: true,
    description: 'ISBN ресурса. Уникальный идентификатор книги',
  })
  @IsOptional()
  @IsString()
  @Validate(IsbnValidator)
  ISBN?: string;

  @Field({ nullable: true, description: 'Ссылка на электронный ресурс' })
  @IsOptional()
  @ValidateIf(
    (item: Item) => (item.isElectronic && !!item.source) || !item.isElectronic,
    { message: 'Ссылка для электронного ресурса обязательная' },
  )
  @IsUrl(null, { message: 'Неккоректная ссылка' })
  @Transform(({ value }) => value.trim())
  source?: string;
}
